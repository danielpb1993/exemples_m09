package prova4_1;

public class SeientsAvio {
    //comencem amb 5 seients lliures a l'avió
    private int seientsLliures = 5;

    public int getSeientsLliures(){
        return seientsLliures;
    }

    public boolean getSeientsLliures(int numPlaces){
        if (numPlaces <= seientsLliures)
            return true;
        else
            return false;
    }


    public void reservaSeients(int numSeientsReserva){
        seientsLliures = seientsLliures - numSeientsReserva;
    }

}
